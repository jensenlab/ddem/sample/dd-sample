import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
// @material-ui/icons

// core components
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
import Parallax from "components/Parallax/Parallax.jsx";
import recordPageStyle from "assets/jss/material-kit-react/views/recordPage.jsx";
import Grid from "@material-ui/core/Grid";

import { OipApi } from "oip/OipApi";
import { config } from "ddx.config.js";

import SnackbarContent from "components/Snackbar/SnackbarContent.jsx";
import { Snackbar } from "@material-ui/core";

const api = new OipApi(config.daemonApiUrl);
const dashboardRoutes = [];

class RecordPage extends React.Component {
  constructor(data) {
    super();
    this.oipref = data.match.params.id;
    this.state = {
      name: "",
      description: "",
      imgRef: require("assets/img/ddx-placeHolder.png"),
      type: "",
      environment: "",
      location: "",
      by: [],
      organizations: [],
      preparedFrom: [],
      protocols: [],
      legacyIds: [],
      error: false,
      errorMessage: "we cannot find parts of this record."
    };
  }

  checkData(id, data) {
    if (data.results.length === 0) {
      return this.setState({ error: true }, () => {
        return false;
      });
    }
    return true;
  }

  basicHandlerOfOipRef = id => {
    console.log(`Receiving: ${id}`);
    return api.getRecord(id).then(data => {
      console.log(`Data received: ${JSON.stringify(data)}`);
      if (!this.checkData(id, data)) {
        return {
          id,
          name: ""
        };
      }
      const name =
        data.results[0].record.details[config.cardInfo.name.tmpl][
          config.cardInfo.name.name
        ];
      return {
        id,
        name
      };
    });
  };

  peopleHandlerOfOipRef = id => {
    console.log(`Receiving: ${id}`);
    return api.getRecord(id).then(data => {
      console.log(`Data received: ${data}`);
      if (!this.checkData(id, data)) {
        return {
          id,
          name: ""
        };
      }
      let name =
        data.results[0].record.details[config.cardInfo.name.tmpl][
          config.cardInfo.name.name
        ];

      if (data.results[0].record.details[config.cardInfo.surname.tmpl]) {
        name +=
          " " +
          data.results[0].record.details[config.cardInfo.surname.tmpl][
            config.cardInfo.surname.name
          ];
      }
      return {
        id,
        name
      };
    });
  };

  componentDidMount() {
    api.getRecord(this.oipref).then(data => {
      if (!this.checkData(this.oipref, data)) {
        return {
          id: this.oipref,
          name: ""
        };
      }
      const recordInfo = data.results[0].record.details;
      if (recordInfo) {
        const avatarId =
          recordInfo[config.cardInfo.avatarRecord.tmpl][
            config.cardInfo.avatarRecord.name
          ];

        if (avatarId) {
          console.log(`avatarId: ${avatarId}`);
          const callAvatar = api.getRecord(avatarId);
          callAvatar.then(avatar => {
            const address =
              avatar.results[0].record.details[config.imageHandler.main.tmpl][
                config.imageHandler.main.name
              ];
            this.setState({
              imgRef: `${config.ipfs.apiUrl}${address}`
            });
          });
        }

        const name =
          recordInfo[config.cardInfo.name.tmpl][config.cardInfo.name.name];

        const description =
          recordInfo[config.cardInfo.description.tmpl][
            config.cardInfo.description.name
          ];

        const type =
          recordInfo[config.sampleHandler.sampleType.tmpl][
            config.sampleHandler.sampleType.name
          ];

        const environment =
          recordInfo[config.sampleHandler.environmentType.tmpl][
            config.sampleHandler.environmentType.name
          ];

        const location =
          recordInfo[config.sampleHandler.location.tmpl][
            config.sampleHandler.location.name
          ];

        const byOipRef =
          recordInfo[config.sampleHandler.by.tmpl][
            config.sampleHandler.by.name
          ];

        const organizationsOipRef =
          recordInfo[config.sampleHandler.organizations.tmpl][
            config.sampleHandler.organizations.name
          ];

        const preparedFromOipRef =
          recordInfo[config.sampleHandler.preparedFrom.tmpl][
            config.sampleHandler.preparedFrom.name
          ];

        const protocolsOipRef =
          recordInfo[config.sampleHandler.protocols.tmpl][
            config.sampleHandler.protocols.name
          ];

        const legacyIdsOipRef =
          recordInfo[config.sampleHandler.legacyIds.tmpl][
            config.sampleHandler.legacyIds.name
          ];

        if (protocolsOipRef) {
          const protocols = protocolsOipRef.map(this.basicHandlerOfOipRef);

          Promise.all(protocols).then(names => {
            this.setState({
              protocols: names
            });
          });
        }

        if (legacyIdsOipRef) {
          const legacyIds = legacyIdsOipRef.map(this.basicHandlerOfOipRef);

          Promise.all(legacyIds).then(names => {
            this.setState({
              legacyIds: names
            });
          });
        }

        if (organizationsOipRef) {
          const organizations = organizationsOipRef.map(
            this.basicHandlerOfOipRef
          );

          Promise.all(organizations).then(names => {
            this.setState({
              organizations: names
            });
          });
        }

        if (preparedFromOipRef) {
          const preparedFrom = preparedFromOipRef.map(
            this.basicHandlerOfOipRef
          );

          Promise.all(preparedFrom).then(names => {
            this.setState({
              preparedFrom: names
            });
          });
        }

        if (byOipRef) {
          const by = byOipRef.map(this.peopleHandlerOfOipRef);

          Promise.all(by).then(names => {
            this.setState({
              by: names
            });
          });
        }

        this.setState({
          name,
          description,
          type,
          environment,
          location
        });
      }
    });
  }

  render() {
    console.log(this.oipref);
    const { classes, ...rest } = this.props;
    return (
      <div>
        <Header
          color="transparent"
          routes={dashboardRoutes}
          brand={config.title}
          rightLinks={<HeaderLinks />}
          fixed
          changeColorOnScroll={{
            height: 400,
            color: "white"
          }}
          {...rest}
        />
        <Parallax className={classes.small}>
          <div className={classes.container}>
            <GridContainer>
              <GridItem xs={12} sm={12} md={6}>
                <h1 className={classes.title}>{this.state.name}</h1>
              </GridItem>
            </GridContainer>
          </div>
        </Parallax>
        <Snackbar
          open={this.state.error}
          autoHideDuration={6000}
          style={{ width: "100%" }}
        >
          <SnackbarContent
            message={
              <span>
                <b>Warning</b> {this.state.errorMessage}
              </span>
            }
            color="danger"
            close
            icon="info_outline"
          />
        </Snackbar>
        <div className={classNames(classes.main, classes.mainRaised)}>
          <div className={classes.container}>
            <Grid container spacing={1}>
              <Grid item xs={12} sm={6} md={4} className={classes.container}>
                <img
                  src={this.state.imgRef}
                  alt="avatar"
                  style={{ maxWidth: "250px" }}
                ></img>
              </Grid>
              <Grid item xs={12} sm={6} md={8}>
                <h3
                  className={classes.title}
                  style={{
                    marginTop: "0px",
                    marginBottom: "-10px"
                  }}
                >
                  {this.state.name}
                </h3>
                <Typography
                  style={{
                    fontSize: "8px",
                    overflowWrap: "break-word",
                    fontStyle: "italic"
                  }}
                  className={classes.root}
                >
                  <Link
                    href={`${config.floExplorer.url}/tx/${this.oipref}`}
                    target="_blank"
                    rel="noreferrer noopener"
                  >
                    {this.oipref}
                  </Link>
                </Typography>
                <p></p>
                <label className={classes.meta}>Description: </label>
                <p>{this.state.description}</p>
                <label className={classes.meta}>Type: </label>
                {this.state.type}
                <br />
                <label className={classes.meta}>Environment: </label>
                <span variant="body2">{this.state.environment}</span>
                <br />
                <label className={classes.meta}>Location: </label>
                <span variant="body2">{this.state.location}</span>
                <br />
                <label className={classes.meta}>Prepared from: </label>
                {this.state.preparedFrom.map((sample, key) => {
                  return (
                    <Link
                      variant="body2"
                      href={`${sample.id}`}
                      target="_blank"
                      rel="noreferrer noopener"
                      key={key}
                    >
                      {sample.name}
                      {key === this.state.preparedFrom.length - 1 ? "." : ", "}
                    </Link>
                  );
                })}
                <br />
                <label className={classes.meta}>Protocols used: </label>
                {this.state.protocols.map((protocol, key) => {
                  return (
                    <Link
                      variant="body2"
                      href={`${config.gateways.protocol}/record/${protocol.id}`}
                      target="_blank"
                      rel="noreferrer noopener"
                      key={key}
                    >
                      {protocol.name}
                      {key === this.state.protocols.length - 1 ? "." : ", "}
                    </Link>
                  );
                })}
                <br />
                <label className={classes.meta}>Prepared by: </label>
                {this.state.by.map((people, key) => {
                  return (
                    <Link
                      variant="body2"
                      href={`${config.gateways.people}/record/${people.id}`}
                      target="_blank"
                      rel="noreferrer noopener"
                      key={key}
                      style={{ paddingRight: "5px" }}
                    >
                      {people.name}
                      {key === this.state.by.length - 1 ? "." : ", "}
                    </Link>
                  );
                })}
                <br />
                <label className={classes.meta}>Organizations: </label>
                {this.state.organizations.map((org, key) => {
                  return (
                    <span
                      /* href={`https://org.oip.io/record/${org.id}`} */
                      target="_blank"
                      rel="noreferrer noopener"
                      key={key}
                      variant="body2"
                      style={{ paddingRight: "5px" }}
                    >
                      {org.name}
                      {key === this.state.organizations.length - 1 ? "." : ", "}
                    </span>
                  );
                })}
              </Grid>
            </Grid>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default withStyles(recordPageStyle)(RecordPage);
