import React from "react";

import { Link } from "react-router-dom";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// core components
import cardStyle from "assets/jss/material-kit-react/views/componentsSections/cardStyle.jsx";
import {
  Box,
  Card,
  CardActionArea,
  CardMedia,
  Typography
} from "@material-ui/core";

import { OipApi } from "oip/OipApi";
import { config } from "ddx.config.js";

const api = new OipApi(config.daemonApiUrl);

class SectionCard extends React.Component {
  state = {
    name: "",
    description: "",
    address: "",
    thumbnail: require("assets/img/ddx-placeHolder.png"),
    txid: ""
  };

  componentDidMount() {
    if (this.props.data) {
      console.log(this.props.data);
      const recordInfo = this.props.data.record.details;
      if (recordInfo) {
        const avatarId =
          recordInfo[config.cardInfo.avatarRecord.tmpl][
            config.cardInfo.avatarRecord.name
          ];
        const callAvatar = api.getRecord(avatarId);

        let name =
          recordInfo[config.cardInfo.name.tmpl][config.cardInfo.name.name];

        const description =
          recordInfo[config.cardInfo.description.tmpl][
            config.cardInfo.description.name
          ];

        const txid = this.props.data.meta.txid;

        this.setState({
          name,
          description,
          txid
        });

        callAvatar.then(response => {
          if (response !== "not found") {
            const address =
              response.results[0].record.details[
                config.imageHandler.thumbnail.tmpl
              ][config.imageHandler.thumbnail.name];
            this.setState({
              thumbnail: `${config.ipfs.apiUrl}${address}`
            });
          }
        });
      }
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <Card className={classes.bullet}>
        <Box display="flex" flexDirection="column" flexGrow={1} height="100%">
          <Box display="flex" flexGrow={1}>
            <Link
              className={classes.cardLink}
              to={"/record/" + this.state.txid}
            >
              <CardActionArea>
                <Box p={1} display="flex" flexGrow={1} width="100%">
                  <Box pr={1} flexDirection="column" flexGrow={1}>
                    <Typography
                      // noWrap={true}
                      variant="h5"
                      // style={{ fontStyle: "bold" }}
                      color="textPrimary"
                    >
                      {this.state.name}
                    </Typography>
                    <Typography
                      // noWrap={true}
                      variant="caption"
                      style={{ overflowWrap: "break-word" }}
                      color="textPrimary"
                    >
                      {this.state.description}
                    </Typography>
                  </Box>

                  <CardMedia
                    component="img"
                    alt="Record Image"
                    className={classes.media}
                    height="150"
                    width="50"
                    image={this.state.thumbnail}
                    title={this.state.name}
                    href="/record"
                  />
                </Box>
              </CardActionArea>
            </Link>
          </Box>

          <Box p={1} display="flex" flexShrink={1} alignItems="flex-end">
            <Box display="inline" maxWidth="100%">
              <Typography
                style={{
                  display: "block",
                  fontSize: "8px",
                  overflowWrap: "break-word",
                  fontStyle: "italic",
                  fontFamily: "courier"
                }}
                color="textPrimary"
              >
                {this.state.txid}
              </Typography>
            </Box>
          </Box>
        </Box>
      </Card>
    );
  }
}

export default withStyles(cardStyle, { withTheme: true })(SectionCard);
