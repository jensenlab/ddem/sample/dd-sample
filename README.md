# DD-Sample

## Install

Pull this repo, navigate to its directory and:

- `npm ci`

## Production

To create a production bundle:

- `npm run build`


## Development

To work in development:

- `npm run start`

## Licensing

- Licensed under MIT
